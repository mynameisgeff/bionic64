#!/bin/sh

parted /dev/sda resizepart 1 100%
pvresize /dev/sda1
lvresize -rl +100%FREE /dev/mapper/vagrant--vg-root
