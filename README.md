# Bionic64
### Set experimental environment variable
`export VAGRANT_EXPERIMENTAL="disks"`

### Usage
`git clone https://gitlab.com/mynameisgeff/bionic64.git`<br>
`cd bionic64`<br>
`vagrant box add hashicorp/bionic64`<br>
`vagrant up --provider=vmware_desktop`
